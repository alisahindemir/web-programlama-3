﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication13.Models;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        DemirResimDbContext context = new DemirResimDbContext();
        // GET: Home
        public ActionResult Index()
        {
            return View(context.HomeSlider.Take(5));
        }
        public ActionResult YeniResimler()
        {
            ViewBag.Message = "Yeni Resimler";
            return View();
        }
        public ActionResult EskiResimler()
        {
            ViewBag.Message = "Eski Resimler";
            return View();
        }
        public ActionResult İletisim()
        {
            ViewBag.Message = "İletişim";
            return View();
        }
        public ActionResult UyeGiris()
        {
            ViewBag.Message = "Giris";
            return View();
        }
        [Authorize]
        public ActionResult AdminPaneli()
        {
            ViewBag.Message = "Admin Paneli";
            return View();
        }
        [Authorize]
        [HttpPost]
        public ActionResult AdminPaneli(HttpPostedFileBase file)
        {
            Slider slide = new Slider();
            if (file != null && file.ContentLength > 0)
            {
                MemoryStream memoryStream = file.InputStream as MemoryStream;
                if (memoryStream == null)
                {
                    memoryStream = new MemoryStream();
                    file.InputStream.CopyTo(memoryStream);
                }
                slide.resim = memoryStream.ToArray();
            }
            context.HomeSlider.Add(slide);
            context.SaveChanges();
            return RedirectToAction("Index", "Home");
        }
    }
}