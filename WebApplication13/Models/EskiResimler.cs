﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class EskiResimler
    {
        public int ID { get; set; }
        public string e_yazi { get; set; }
        public virtual ICollection<Yorum> Yorumlar { get; set; }
    }
}