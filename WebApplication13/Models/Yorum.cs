﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class Yorum
    {
        public int ID { get; set; }
        public string k_adi { get; set; }
        public string y_yazı { get; set; }
        public virtual YeniResimler y_Resim { get; set; }
        public virtual EskiResimler e_Resim { get; set; }
    }
}