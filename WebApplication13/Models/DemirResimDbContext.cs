﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplication13.Models
{
    public class DemirResimDbContext:DbContext
    {
        public DemirResimDbContext()
            : base("DefaultConnection")
        {
        }
        public DbSet<Slider> HomeSlider { get; set; }
    }
}